var AnonymToken = artifacts.require("./token/AnonymToken.sol");
var Crowdsale = artifacts.require("./crowdsale/Crowdsale.sol");

module.exports = function(deployer) {
  deployer.deploy(AnonymToken);
  deployer.deploy(Crowdsale);
};
