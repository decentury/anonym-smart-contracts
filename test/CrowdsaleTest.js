const Crowdsale = artifacts.require('Crowdsale');
const AnonymToken = artifacts.require('AnonymToken');

contract('Crowdsale', function (accounts) {
  let investor = accounts[0];
  let beneficiary = accounts[1];
  let crowdsaleOwner = accounts[2];
  let tokenOwner = accounts[3];
  let account = accounts[4];

  let token;
  let crowdsale;

  async function checkContractOwner(contractInstance, expectedOwner){
    let ownerCall = await contractInstance.owner.call();
    assert.equal(ownerCall.valueOf(), expectedOwner, 'The owner in the contract does not match the value that is set in the settings');
  };

  async function setState(state){
    let currentState = await crowdsale.currentState();
    for(let i = currentState.toNumber() + 1; i <= state; i++) {
      await crowdsale.setState(i, {from: crowdsaleOwner});
    }
    currentState = await crowdsale.currentState();
    assert.equal(currentState.toNumber(), state);
  };

  async function checkCallFnNotOwner(fn, nameFn, nameOwner, ...args) {
    try {
      await fn(...args, {from: account});

      assert.fail(0, 0, "{0} can be called not by {1}".format(nameFn, nameOwner));
    } catch (e){
    };
  }

  async function checkCallFnInState(fn, nameFn, state, ...args) {
    try {
      await fn(...args, {from: account});

      assert.fail(0, 0, "{0} can be called in state {1}".format(nameFn, State[state]));
    } catch (e){
    };
  }

  async function checkCallInternalFn(fn, nameFn, ...args) {
    try {
      await fn(...args, {from: account});

      assert.fail(0, 0, "{0} is not internal function".format(nameFn));
    } catch (e){
    };
  }


  beforeEach(async function () {
    token = await AnonymToken.new({from: tokenOwner});
    crowdsale = await Crowdsale.new(beneficiary, {from: crowdsaleOwner});

    await crowdsale.initialize(token.address, {from: crowdsaleOwner});

    await token.setCrowdsaleMinter(crowdsale.address, {from: tokenOwner});
  });

  it('the contract owner must be correctly set', async function() {
    await checkContractOwner(crowdsale, crowdsaleOwner);
    await checkContractOwner(token, tokenOwner);
  });

  it('token minter must be set correctly', async function () {
    const minter = await token.crowdsaleMinter();
    assert.equal(minter, crowdsale.address);
  });

  it('token must be set correctly', async function () {
    const crowdsaleToken = await crowdsale.token();
    assert.equal(crowdsaleToken, token.address);
  });

  it('_mint, _finish, _checkMaxSaleSupply must be internal function', async function(){
    await checkCallInternalFn(crowdsale._mint, 'crowdsale._mint', 0, investor, 100);
    await checkCallInternalFn(crowdsale._finish, 'crowdsale._finish', 0);
    await checkCallInternalFn(crowdsale._checkMaxSaleSupply, 'crowdsale._checkMaxSaleSupply', 0);
  });

   it('new bonus must be set correctly', async function () {
     let bonus = await crowdsale.bonus();
     assert.equal(bonus, 5000);

     let newBonus = 120000;
     await crowdsale.setBonus(newBonus, {from: crowdsaleOwner});
     bonus = await crowdsale.bonus();

     assert.equal(bonus, newBonus);
   });

   it('new token price must be set correctly', async function () {
     let tokenPrice = await crowdsale.currentPrice();
     assert.equal(tokenPrice, 1 * (10**18));

     let newTokenPrice = 2 * (10**18);
     await crowdsale.setTokenPrice(newTokenPrice, {from: crowdsaleOwner});
     tokenPrice = await crowdsale.currentPrice();
     assert.equal(tokenPrice, newTokenPrice);
  });

  it('should set correct new state', async function () {
    let state = await crowdsale.currentState();
    assert.equal(state, 0);

    for(let i = 1; i < 8; i++) {
      await crowdsale.setState(i, {from: crowdsaleOwner});
      state = await crowdsale.currentState();
      assert.equal(state, i);
    }
  });

  it('should not set new state', async function () {
    let state = await crowdsale.currentState();
    assert.equal(state, 0);

    for(let i = 1; i < 5; i++) {
      try {
        await crowdsale.setState(i + 1, {from: crowdsaleOwner});
      } catch (error) {
        const invalidOpcode = error.message.search('invalid opcode') >= 0;
        const outOfGas = error.message.search('out of gas') >= 0;
        assert(
          invalidOpcode || outOfGas,
          "Expected throw, got '" + error + "' instead",
        );
      }
      await crowdsale.setState(i, {from: crowdsaleOwner});
    }
  });

  it('should set STOPPED state', async function () {
    let state = await crowdsale.currentState();
    assert.equal(state, 0);

    await crowdsale.setState(1, {from: crowdsaleOwner});
    state = await crowdsale.currentState();
    assert.equal(state, 1);

    await crowdsale.setState(7, {from: crowdsaleOwner});
  });


//--------------------Calling functions in incorrect state----------------

  it('Calling functions in State.INIT', async function () {
    await checkCallFnInState(crowdsale.refund, 'crowdsale.mintPresale', 0, account, 100);
    await checkCallFnInState(crowdsale.send, 'crowdsale.send', 0, '5000000000000000000');
    await checkCallFnInState(crowdsale.investDirect, 'crowdsale.investDirect', 0, account, 100, {from: crowdsaleOwner});
  });

  it('Calling functions in State.PRESALE', async function () {
    await setState(1);

    await checkCallFnInState(crowdsale.initialize, 'crowdsale.initialize', 1, account);
    await checkCallFnInState(crowdsale.refund, 'crowdsale.mintPresale', 0, account, 100);
    await checkCallFnInState(crowdsale.send, 'crowdsale.send', 1, '5000000000000000000');
    await checkCallFnInState(crowdsale.investDirect, 'crowdsale.investDirect', 1, account, 100, {from: crowdsaleOwner});
  });

  it('Calling functions in State.PREICO', async function () {
    await setState(2);

    await checkCallFnInState(crowdsale.initialize, 'crowdsale.initialize', 2, account);
    await checkCallFnInState(crowdsale.refund, 'crowdsale.mintPresale', 0, account, 100);
  });

  it('Calling functions in State.PREICO_FINISHED', async function () {
    await setState(3);

    await checkCallFnInState(crowdsale.initialize, 'crowdsale.initialize', 2, account);
    await checkCallFnInState(crowdsale.refund, 'crowdsale.mintPresale', 0, account, 100);
    await checkCallFnInState(crowdsale.send, 'crowdsale.send', 1, '5000000000000000000');
    await checkCallFnInState(crowdsale.investDirect, 'crowdsale.investDirect', 1, account, 100, {from: crowdsaleOwner});
  });

  it('Calling functions in State.ICO', async function () {
    await setState(4);

    await checkCallFnInState(crowdsale.initialize, 'crowdsale.initialize', 2, account);
    await checkCallFnInState(crowdsale.refund, 'crowdsale.mintPresale', 0, account, 100);
  });

  it('Calling functions in State.CLOSED', async function () {
    await setState(6);

    await checkCallFnInState(crowdsale.initialize, 'crowdsale.initialize', 2, account);
    await checkCallFnInState(crowdsale.refund, 'crowdsale.mintPresale', 0, account, 100);
    await checkCallFnInState(crowdsale.send, 'crowdsale.send', 1, '5000000000000000000');
    await checkCallFnInState(crowdsale.investDirect, 'crowdsale.investDirect', 1, account, 100, {from: crowdsaleOwner});
  });

  it('Calling functions in State.STOPPED', async function () {
    await setState(7);

    await checkCallFnInState(crowdsale.initialize, 'crowdsale.initialize', 2, account);
    await checkCallFnInState(crowdsale.setBonus, 'crowdsale.setBonus', 0, {from: crowdsaleOwner});
    await checkCallFnInState(crowdsale.setTokenPrice, 'crowdsale.setTokenPrice', 0, {from: crowdsaleOwner});
    await checkCallFnInState(crowdsale.refund, 'crowdsale.mintPresale', 0, account, 100);
    await checkCallFnInState(crowdsale.send, 'crowdsale.send', 1, '5000000000000000000');
    await checkCallFnInState(crowdsale.investDirect, 'crowdsale.investDirect', 1, account, 100, {from: crowdsaleOwner});
  });


//--------------------------------------------------------------------------

  it("Direct user's purchases through the contract", async function() {
    await setState(4);
    await crowdsale.send('5000000000000000000');

    let balance = await token.balanceOf(investor);
    assert.equal(balance, 7500000000000000000 , "Tokens are not credited to the user's balance");

    let refundBalance = await crowdsale.refundBalance(investor);
    assert.equal(refundBalance, '5000000000000000000' , 'User investment is not saved');

    const post = web3.eth.getBalance(crowdsale.address);
    assert.equal(post, '5000000000000000000');
  });

  it('Direct investment', async function() {
    await setState(4);
    let tx = await crowdsale.investDirect(investor, 10, {from: crowdsaleOwner});

    let balance = await token.balanceOf(investor);
    assert.equal(balance, 10 , "Tokens are not credited to the user's balance");
  });

  it("Refund user's investment", async function() {
    await setState(4);
    await crowdsale.send('5000000000000000000');

    let balance = await token.balanceOf(investor);
    assert.equal(balance, '7500000000000000000' , "Tokens are not credited to the user's balance");

    let refundBalance = await crowdsale.refundBalance(investor);
    assert.equal(refundBalance, '5000000000000000000' , 'User investment is not saved');

    const pre = web3.eth.getBalance(investor);
    await setState(5);
    await crowdsale.refund({from: investor});

    refundBalance = await crowdsale.refundBalance(investor);
    assert.equal(refundBalance, '0' , 'User investment is not refunded');
    const post = web3.eth.getBalance(investor);
    assert.isAbove(post, pre);
  });

  it("Withdraw beneficiary's coins", async function() {
    await setState(4);
    await crowdsale.send('5000000000000000000');

    let startCrowdsaleBalance = web3.eth.getBalance(crowdsale.address);
    assert.equal(startCrowdsaleBalance, '5000000000000000000');
    let startBeneficiaryBalance =  web3.eth.getBalance(beneficiary);

    await crowdsale.withdraw('5000000000000000000', {from: crowdsaleOwner});

    let crowdsaleBalance = web3.eth.getBalance(crowdsale.address);
    assert.equal(startCrowdsaleBalance.toNumber() - 5000000000000000000, crowdsaleBalance.toNumber());

    let beneficiaryBalance =  web3.eth.getBalance(beneficiary);
    assert.equal(startBeneficiaryBalance.toNumber() + 5000000000000000000, beneficiaryBalance.toNumber());
  });
})
