pragma solidity ^0.4.15;

import '../lib/SafeMath.sol';
import '../lib/Base.sol';
import '../lib/Owned.sol';


contract IToken {
    function mint(address _to, uint _amount) public returns(bool);
    function start() public;
}

contract Crowdsale is Base, Owned {
    using SafeMath for uint256;

    enum State { INIT, PRESALE, PREICO, PREICO_FINISHED, ICO, REFUND_RUNNING, CLOSED, STOPPED }
    uint public constant MAX_PREICO_SUPPLY = 3000000 * (10**18);
    uint public constant MAX_ICO_SUPPLY = 100000000 * (10**18);
    uint public constant DECIMALS = 10**18;

    State public currentState = State.INIT;
    IToken public token;
    uint public totalPreICOSupply = 0;
    uint public totalICOSupply = 0;
    uint public totalFunds = 0;
    uint public tokenPrice = 1 * (10**18); //wei
    uint public bonus = 5000; //50%
    uint public currentPrice;
    address public beneficiary;
    mapping(address => uint) balances;
    uint public countMembers = 0;

    uint private bonusBase = 10000; //100%;

    event Transfer(address indexed _to, uint256 _value);

    modifier inState(State _state){
        require(currentState == _state);
        _;
    }

    modifier salesRunning(){
        require(currentState == State.PREICO || currentState == State.ICO);
        _;
    }

    modifier notStopped(){
        require(currentState != State.STOPPED);
        _;
    }

    function Crowdsale(address _beneficiary) public {
        beneficiary = _beneficiary;
    }

    function ()
        public
        payable
        salesRunning
    {
        _receiveFunds();
    }

    function initialize(address _token)
        public
        onlyOwner
        inState(State.INIT)
    {
        require(_token != address(0));

        token = IToken(_token);
        currentPrice = tokenPrice;
    }

    function setBonus(uint _bonus) public
        onlyOwner
        notStopped
    {
        bonus = _bonus;
    }

    function setTokenPrice(uint _tokenPrice) public
        onlyOwner
        notStopped
    {
        currentPrice = _tokenPrice;
    }

    function setState(State _newState)
        public
        onlyOwner
    {
        require(
            currentState != State.STOPPED && (_newState == State.STOPPED ||
            (currentState == State.INIT && _newState == State.PRESALE
            || currentState == State.PRESALE && _newState == State.PREICO
            || currentState == State.PREICO && _newState == State.PREICO_FINISHED
            || currentState == State.PREICO_FINISHED && _newState == State.ICO
            || currentState == State.ICO && (_newState == State.REFUND_RUNNING || _newState == State.CLOSED)
            || currentState == State.REFUND_RUNNING && _newState == State.CLOSED))
        );

        if(_newState == State.CLOSED){
            _finish();
        }

        currentState = _newState;
    }

    function refundBalance(address _owner)
      public
      constant
      returns(uint)
    {
      return balances[_owner];
    }

    function withdraw(uint _amount)
        public
        noAnyReentrancy
        onlyOwner
    {
        require(_amount > 0 && _amount <= this.balance);
        beneficiary.transfer(_amount);
    }

    function refund()
        public
        noAnyReentrancy
        inState(State.REFUND_RUNNING)
    {
        require(balances[msg.sender] != 0);
        uint amountToRefund = balances[msg.sender];
        balances[msg.sender] = 0;

        msg.sender.transfer(amountToRefund);
    }

    function investDirect(address _to, uint _amount)
        public
        onlyOwner
        salesRunning
    {
        _checkMaxSaleSupply(_amount);

        _mint(_to, _amount);
        Transfer(_to, _amount);
    }

    function getCountMembers()
    public
    constant
    returns(uint)
    {
        return countMembers;
    }

    //==================== Internal Methods =================
    function _mint(address _to, uint _amount)
        noAnyReentrancy
        internal
    {
        IToken(token).mint(_to, _amount);
    }

    function _finish()
        noAnyReentrancy
        internal
    {
        IToken(token).start();
    }

    function _receiveFunds()
    internal
    {
        require(msg.value != 0);
        uint weiAmount = msg.value;
        uint transferTokens = weiAmount.mul(DECIMALS).div(currentPrice);

        _checkMaxSaleSupply(transferTokens);

        if(balances[msg.sender] == 0){
            countMembers = countMembers.add(1);
        }

        uint bonusTokens = transferTokens.mul(bonus).div(bonusBase);
        transferTokens = transferTokens.add(bonusTokens);

        balances[msg.sender] = balances[msg.sender].add(weiAmount);
        totalFunds = totalFunds.add(weiAmount);

        _mint(msg.sender, transferTokens);
        Transfer(msg.sender, transferTokens);
    }

    function _checkMaxSaleSupply(uint transferTokens) internal {
        if(currentState == State.PREICO) {
            require(totalPreICOSupply.add(transferTokens) <= MAX_PREICO_SUPPLY);
            totalPreICOSupply = totalPreICOSupply.add(transferTokens);
        } else if(currentState == State.ICO) {
            require(totalICOSupply.add(transferTokens) <= MAX_ICO_SUPPLY);
            totalICOSupply = totalICOSupply.add(transferTokens);
        }
    }
}
